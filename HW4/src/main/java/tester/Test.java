package tester;

import javax.swing.JOptionPane;

import account.Account;
import account.Person;
import account.SavingAccount;
import account.SpendingAccount;
import bank.Bank;
import junit.framework.TestCase;
import start.LoadSave;

public class Test extends TestCase {
	  
	public void test()
	{
		Bank bank = new Bank();
		/*
	    bank.addPerson("Eren Jäeger",1);
		bank.addPerson("Sherlock Holmes",2);
		bank.addPerson("Levi Ackerman",3);
		bank.addPerson("Dorian Grey",4 );
		bank.addPerson("Lia Overbrook",5 );
		bank.addPerson("Alan Turing",6 );
		bank.addPerson("Edward Elric",7 );
		bank.addPerson("L",8 );
		bank.addPerson("Armin Arlert",9 );
		bank.addPerson("Mikasa Ackerman",10 );
		bank.addPerson("Hanji Zoe",11 );
		bank.addPerson("Erwin Smith",12 );
		bank.addPerson("Jean Kirchstein",13 );
		bank.addPerson("Historia Reiss",14 );
		bank.addAccountForPerson(1,true,"Eren Jäeger",1);
        bank.addAccountForPerson(2,false,"Eren Jäeger",1);
	    bank.addAccountForPerson(3,false,"Sherlock Holmes",2);
	    bank.addAccountForPerson(4,true,"Sherlock Holmes",2);
        bank.addAccountForPerson(5,true,"Sherlock Holmes",2);
	    bank.addAccountForPerson(6,false,"Levi Ackerman",3);
	    bank.addAccountForPerson(7,true,"Lia Overbrook",5);
        bank.addAccountForPerson(8,true,"L",8);
	    bank.addAccountForPerson(9,false,"L",8);
	    bank.addAccountForPerson(10,true,"Mikasa Ackerman",10);
        bank.addAccountForPerson(11,true,"Erwin Smith",12);
	    bank.addAccountForPerson(12,false,"Erwin Smith",12);
	    bank.addAccountForPerson(13,true,"Historia Reiss",14);
        bank.addAccountForPerson(14,true,"Armin Arlert",9);
	    bank.addAccountForPerson(15,false,"Historia Reiss",14);
	    bank.addAccountForPerson(16,true,"Historia Reiss",14);
        bank.addAccountForPerson(17,true,"Historia Reiss",14);
	    bank.addAccountForPerson(18,false,"Hanji Zoe",11);
	    bank.addAccountForPerson(19,true,"Dorian Grey",4);
        bank.addAccountForPerson(20,true,"Alan Turing",6);
	    bank.addAccountForPerson(21,false,"Jean Kirchstein",13);
		bank.generateReport();
		bank.deletePerson("Jean Kirchstein",13);
		bank.deletePerson("Erwin Smith",12);
		bank.deletePerson("Armin Arlert",9);
		bank.generateReport();
		bank.deleteAccountForPerson(16,"Historia Reiss",14);
		bank.deleteAccountForPerson(4,"Sherlock Holmes",2);
		bank.deleteAccountForPerson(20,"Alan Turing",6);
		bank.deleteAccountForPerson(8,"L",8);
		bank.generateReport();
		LoadSave.save(bank.getBank(), "bank.txt");
		bank.setBank(LoadSave.load("bank.txt"));
		bank.generateReport();
		*/
		   
			bank.setBank(LoadSave.load("bank.txt"));
		    for(Account ac: bank.getAllAccountsForPerson("L",8))
			{ 
				if(ac.getId() == 9)
				{
					if (ac instanceof SpendingAccount )
					{
						ac.withdrawMoney(100);
					}
					else{
						SavingAccount sa = (SavingAccount)ac; 
						if (!sa.isWithdrawn())
						{
							sa.withdrawMoney(100);
							JOptionPane.showMessageDialog(null, "The withdrawn sum is " + sa.calculateMoneyWithdrawal());
						}
						else
							JOptionPane.showMessageDialog(null, "Cannot withdraw money more than once");
							
					}
					break;
				}
				
			}
		  
		    bank.generateReport();
		    bank.deleteAccountForPerson(21, "Jean Kirchstein",13);
		    bank.generateReport();
		    bank.deletePerson("Jean Kirchstein", 13);
		    bank.generateReport();
		    //LoadSave.save(bank.getBank(), "bank.txt");
	}
}
