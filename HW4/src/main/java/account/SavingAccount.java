package account;

public class SavingAccount extends Account{

	private double interestRate = 0.2;
    private boolean deposited;
    private boolean withdrawn;
    
	public SavingAccount(int i)
	{
		super(i);
		deposited = false;
		withdrawn = false;
	}
	
    @Override
	public void depositMoney(double d)
	{
    	if(deposited == false)
    	{
    		this.balance += d;
    		this.deposited = true;
    		setChanged();
    		notifyObservers(this.balance);
    	}
	}
	
	public void withdrawMoney()
	{
    	
    	if(withdrawn == false)
    	{
    		this.balance = 0.0;
    		this.withdrawn = true;
    		setChanged();
    		notifyObservers(this.balance);
    	}
	}
	public double calculateInterest()
	{
		if(deposited)
		{
			return 0.02*balance;
		}
		else return 0;
	}
	
	public double calculateMoneyWithdrawal()
	{
		return this.balance+calculateInterest();   //returns the current balance plus the interest.
	}
	
	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public String toString()
	{
		return "(id: " + id + "; balance: " + balance + "; interest rate: " + interestRate + ")";
	}

	public boolean isDeposited() {
		return deposited;
	}

	public void setDeposited(boolean deposited) {
		this.deposited = deposited;
	}

	public boolean isWithdrawn() {
		return withdrawn;
	}

	public void setWithdrawn(boolean withdrawn) {
		this.withdrawn = withdrawn;
	}

	
}
