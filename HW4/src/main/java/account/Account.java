package account;

import java.io.Serializable;
import java.util.Observable;

public class Account extends Observable implements Serializable{
	private static final long serialVersionUID = 1L;
	protected int id;
	protected double balance;
	
	public Account(int i) {
		this.id = i;
		balance = 0.0;
	}

	public void depositMoney(double d)
	{
		this.balance += d;
		setChanged();
		notifyObservers(this.balance);
	}
	
	public void withdrawMoney(double d)
	{
		this.balance -= d;
		setChanged();
		notifyObservers(this.balance);
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
			return true;  //same instance
		if(o == null) return false;
		if(!(o instanceof Account)) return false;
		if (this.id !=((Account) o).getId()) return false;    //logic equality for the id field
		//if (this.balance != ((Account)o).getBalance()) return false; //logic equality for the balance field
		return true;
	}
	
	@Override
	public int hashCode()
	{
		return ((Integer)id).hashCode();
	}
	
	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
