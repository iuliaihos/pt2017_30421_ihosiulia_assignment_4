package account;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JOptionPane;

public class Person implements Serializable, Observer
{
	
	private static final long serialVersionUID = 1L;
	private String name;
	private int id;
	
	public Person(String name, int id)
	{
		this.name = name;
		this.id = id;
	}
	
    @Override
	public int hashCode()
	{
		int code = 0;
		if(name != null)
			code +=name.hashCode();
		code += id;
		return code;
	}
    
	@Override
	public boolean equals(Object o)
	{
		if (this == o)
			return true;  //same instance
		if(o == null) return false;
		if(!(o instanceof Person)) return false;
		Person p = (Person) o;
		if(!this.name.equals(p.getName())) return false;  //logic equality for the name field
		if (this.id != p.getId()) return false;    //logic equality for the id field
		return true;
	}
	
	public void update(Observable obs, Object obj) {
		if(obj instanceof Double)
			{
			JOptionPane.showMessageDialog(null, getName() + ": BalanceObserver: changed to: " + ((Double)obj).doubleValue());
			System.out.println(getName() + ": BalanceObserver: changed to: " + ((Double)obj).doubleValue());
			}
		else
		{
			System.out.println(getName() + ": BalanceObserver: other changes");
			JOptionPane.showMessageDialog(null, getName() + ": BalanceObserver: other changes");
		}
		
	}
	
	public String toString()
	{
		String s = "";
		s += id + ". " + name + "\n";
		return s;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
