package account;

public class SpendingAccount extends Account{

	
	public SpendingAccount( int i)
	{
		super(i);
	}

	public String toString()
	{
		return "(id: " + id + "; balance: " + balance + ")";
	}
	}
