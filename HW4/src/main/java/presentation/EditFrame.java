package presentation;

import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import account.Account;
import account.Person;
import account.SpendingAccount;
import account.SavingAccount;
import bank.Bank;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;

public class EditFrame {

		@SuppressWarnings("unused")
		private static final long serialVersionUID = 1L;
		  private JFrame frame;
		  private JPanel topMost;
		  private JPanel op;
		  private JTextField pName;
		  private JTextField pId;
		  private JTextField aId;
		  private JTextField aSum;
		  private JButton withdraw;
		  private JButton viewBalance;
		  private JButton viewInterest;
		  private JButton deposit;
		  private JButton viewAccountsForPerson;
		  private Bank bank;
		  //private Person accHolder;
		
		 
		  
		 
		public EditFrame(Bank b)
		  {
			  
			  bank = b;
			  frame = new JFrame("Bank");
			  frame.setSize(800,210);
			  //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			  
			  op = new JPanel();
			  topMost = new JPanel();
			  
			  viewAccountsForPerson = new JButton("View all accounts belonging to:");
			  pName = new JTextField("           name             ");
			  pId = new JTextField("person_id");
			  pName.setFont(new Font("Sans Serif", Font.BOLD,20));
			  pId.setFont(new Font("Sans Serif", Font.BOLD,20));
			 
			  viewAccountsForPerson.addActionListener(
					  new ActionListener()
					  {
						public void actionPerformed(ActionEvent ae) {
							String name = pName.getText();
							int id = Integer.parseInt(pId.getText());
							String s = "";
							for(Account a: bank.getAllAccountsForPerson(name,id))
							{
								s+=a.toString()+"\n";
							}
						JOptionPane.showMessageDialog(null, s);
						}  
					  }
					  );
			  
			  topMost.add(viewAccountsForPerson);
			  topMost.add(pName);
			  topMost.add(pId);
			 
			  withdraw = new JButton("Withdraw");
			  deposit = new JButton("Deposit");
			  viewBalance = new JButton("View balance");
			  viewInterest = new JButton("View interest");
			  aId = new JTextField("acc_id ");
			  aSum = new JTextField("     0.0     ");
			  aId.setFont(new Font("Sans Serif", Font.BOLD,20));
			  aSum.setFont(new Font("Sans Serif", Font.BOLD,20));
			  withdraw.addActionListener(
					  new ActionListener()
					  {
						public void actionPerformed(ActionEvent ae) {
							String name = pName.getText();
							int id = Integer.parseInt(pId.getText());
							int acId = Integer.parseInt(aId.getText());
							double amount = Double.parseDouble(aSum.getText());
							for(Account ac: bank.getAllAccountsForPerson(name,id))
							{ 
								if(ac.getId() == acId)
								{
									if (ac instanceof SpendingAccount )
									{
										ac.withdrawMoney(amount);
									}
									else{
										SavingAccount sa = (SavingAccount)ac; 
										if (!sa.isWithdrawn())
										{
											sa.withdrawMoney(amount);
											JOptionPane.showMessageDialog(null, "The withdrawn sum is " + sa.calculateMoneyWithdrawal());
										}
										else
											JOptionPane.showMessageDialog(null, "Cannot withdraw money more than once");
											
									}
									break;
								}
								
							}
						}  
					  }
					  );
			  deposit.addActionListener(
					  new ActionListener()
					  {
						  public void actionPerformed(ActionEvent ae) {
							    String name = pName.getText();
								int id = Integer.parseInt(pId.getText());
								int acId = Integer.parseInt(aId.getText());
								double amount = Double.parseDouble(aSum.getText());
								for(Account ac: bank.getAllAccountsForPerson(name,id))
								{
									if(ac.getId() == acId)
										{
										if (ac instanceof SpendingAccount )
										{ac.depositMoney(amount);
										break;}
										else{
											if (!((SavingAccount)ac).isDeposited())
											{
												ac.depositMoney(amount);
												((SavingAccount)ac).setDeposited(true);
												break;
											}
											else
												JOptionPane.showMessageDialog(null, "Cannot deposit money more than once");
												
										}
										}
								}
							}  
					  }
					  );
			  viewBalance.addActionListener(
					  new ActionListener()
					  {
						  public void actionPerformed(ActionEvent ae) {
							   String name = pName.getText();
								int id = Integer.parseInt(pId.getText());
								int acId = Integer.parseInt(aId.getText());
								for(Account a: bank.getAllAccountsForPerson(name,id))
								{
									if(a.getId() == acId)
									{
										JOptionPane.showMessageDialog(null, "The balance is: "+a.getBalance());
										break;
									}
										
								}
							}  
					  }
					  );
			  viewInterest.addActionListener(
					  new ActionListener()
					  {
						  public void actionPerformed(ActionEvent ae) {
							    String name = pName.getText();
								int id = Integer.parseInt(pId.getText());
								int acId = Integer.parseInt(aId.getText());
								for(Account a: bank.getAllAccountsForPerson(name, id))
								{
									if(a.getId() == acId)
									{
										if(a instanceof SavingAccount)
										{
											JOptionPane.showMessageDialog(null, "The calculated interest is: "+ ((SavingAccount) a).calculateInterest());
											break;
										}
										else
										{
											JOptionPane.showMessageDialog(null, "This is a spending account");
											break;
										}
									}
										
								}
							}  
					  }
					  );
			  op.add(withdraw);
			  op.add(deposit);
			  op.add(viewBalance);
			  op.add(viewInterest);
			  op.add(aId);
			  op.add(aSum);
			  frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.X_AXIS));
			
			  frame.getContentPane().add(topMost);
			  frame.getContentPane().add(op);
			
			  frame.setVisible(true);
			  
			  
		  }

}
