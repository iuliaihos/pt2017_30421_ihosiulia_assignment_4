package presentation;

import java.awt.Font;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;
import java.lang.Class;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;

import account.Account;
import account.Person;
import account.SavingAccount;
import account.SpendingAccount;
import bank.Bank;
import start.LoadSave;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

public class Frame {
	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;
	  private JFrame frame;
	  private JPanel topMost;
	  private JPanel pers;
	  private JPanel acc;
	  private JPanel pers1;
	  private JTextField pName;
	  private JTextField pId;
	  private JTextField aPName;
	  private JTextField aPId;
	  private JTextField aId;
	
	  private JScrollPane persons;
	  private JScrollPane accounts;
	  private JButton addPerson;
	  private JButton removePerson;
	  private JButton edit;
	  private JButton viewPersons;
	  private JButton genStat;
	  private JButton addAccount;
	  private JButton removeAccount;
	  private JButton viewAccounts;
	  private JCheckBox saving;
	  private JCheckBox spending;
	  private JTable pTable;
	  private JTable aTable;
	  private Bank bank;
	  @SuppressWarnings("rawtypes")
	  private Class c;
	  private JButton save;
	 
	 
	  
	 
	public Frame()
	  {
         bank = new Bank();
		 bank.setBank(LoadSave.load("bank.txt"));
		  
		  
		  frame = new JFrame("Bank");
		  pers = new JPanel();
		  acc = new JPanel();
		  topMost = new JPanel();
		  JLabel intro = new JLabel();
		  intro.setText("Welcome!");
		  edit = new JButton("Account operations");
		  genStat = new JButton("View statistics");
		  genStat.addActionListener(
				  new ActionListener()
				  {
					public void actionPerformed(ActionEvent ae) {
						bank.generateReport();
					}  
				  }
				  );
		 
		  frame.setSize(900,900);
		  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		  accounts = new JScrollPane();
		  edit.addActionListener(
				  new ActionListener()
				  {
					@SuppressWarnings("unused")
					public void actionPerformed(ActionEvent ae) {
						EditFrame ef = new EditFrame(bank);
					}
					  
				  }
				  );
		  
		 
		  
		  addAccount = new JButton("Add a new account");
		  removeAccount = new JButton("Remove an account");
		  
		  addAccount.addActionListener(
				  new ActionListener()
				  {
					public void actionPerformed(ActionEvent ae) {
						String name = aPName.getText();
						int id = Integer.parseInt(aPId.getText());
						int accId = Integer.parseInt(aId.getText());
						if (spending.isSelected())
						{
							bank.addAccountForPerson(accId, true, name,id);
						}
						else if (saving.isSelected())
						{
							bank.addAccountForPerson(accId,false, name,id);
						}
		
					}
					  
				  }
				  );
		  
		  removeAccount.addActionListener(
				  new ActionListener()
				  {
					public void actionPerformed(ActionEvent ae) {
						String name = aPName.getText();
						int id = Integer.parseInt(aPId.getText());
						int accId = Integer.parseInt(aId.getText());
						bank.deleteAccountForPerson(accId, name,id);
				
					}
					  
				  }
				  );
		  frame.getContentPane().setLayout(new GridLayout(0, 1, 0, 0));

		  frame.getContentPane().add(topMost);
		  save = new JButton("Save");
		  save.addActionListener(new ActionListener() {
		  	public void actionPerformed(ActionEvent arg0) {
		  		LoadSave.save(bank.getBank(),"bank.txt");
		  	}
		  });
		  GroupLayout gl_topMost = new GroupLayout(topMost);
		  gl_topMost.setHorizontalGroup(
		  	gl_topMost.createParallelGroup(Alignment.LEADING)
		  		.addGroup(gl_topMost.createSequentialGroup()
		  			.addGroup(gl_topMost.createParallelGroup(Alignment.LEADING)
		  				.addGroup(gl_topMost.createSequentialGroup()
		  					.addGap(286)
		  					.addComponent(genStat)
		  					.addGap(18)
		  					.addComponent(edit))
		  				.addGroup(gl_topMost.createSequentialGroup()
		  					.addGap(386)
		  					.addComponent(intro)))
		  			.addPreferredGap(ComponentPlacement.RELATED, 156, Short.MAX_VALUE)
		  			.addComponent(save, GroupLayout.PREFERRED_SIZE, 91, GroupLayout.PREFERRED_SIZE)
		  			.addGap(27))
		  );
		  gl_topMost.setVerticalGroup(
		  	gl_topMost.createParallelGroup(Alignment.TRAILING)
		  		.addGroup(gl_topMost.createSequentialGroup()
		  			.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
		  			.addComponent(intro)
		  			.addGap(18)
		  			.addGroup(gl_topMost.createParallelGroup(Alignment.BASELINE)
		  				.addComponent(edit)
		  				.addComponent(genStat))
		  			.addGap(101))
		  		.addGroup(Alignment.LEADING, gl_topMost.createSequentialGroup()
		  			.addGap(29)
		  			.addComponent(save, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
		  			.addContainerGap(89, Short.MAX_VALUE))
		  );
		  topMost.setLayout(gl_topMost);
		  frame.getContentPane().add(pers);
		  pers1 = new JPanel();
		  pName = new JTextField("name                               ");
		  pId = new JTextField("id   ");
		  pName.setFont(new Font("Sans Serif", Font.BOLD,20));
		  pId.setFont(new Font("Sans Serif", Font.BOLD,20));
		  pers1.add(pName);
		  pers1.add(pId);
		  viewPersons = new JButton("View all account holders");
		  
		  viewPersons.addActionListener(
				  new ActionListener()
				  {
					public void actionPerformed(ActionEvent ae) {
						Set<Object> o = new HashSet<Object>();
						for(Person p : bank.getAllPersons())
						{
							o.add((Object)p);
						}
						pTable.setModel(createTable(o));
					}
					  
				  }
				  );
		  removePerson = new JButton("Remove an account holder");
		  
		  removePerson.addActionListener(
				  new ActionListener()
				  {
					public void actionPerformed(ActionEvent ae) {
						String name = pName.getText();
						int id = Integer.parseInt(pId.getText());
						bank.deletePerson(name,id);
					}  
				  }
				  );
		  
		   addPerson = new JButton("Add a new account holder");
		   addPerson.addActionListener(
				  new ActionListener()
				  {
					public void actionPerformed(ActionEvent ae) {
						String name = pName.getText();
						int id = Integer.parseInt(pId.getText());
						bank.addPerson(name,id);
					}  
				  }
				  );
		  GroupLayout gl_pers = new GroupLayout(pers);
		  gl_pers.setHorizontalGroup(
		  	gl_pers.createParallelGroup(Alignment.LEADING)
		  		.addGroup(gl_pers.createSequentialGroup()
		  			.addGap(47)
		  			.addComponent(pers1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
		  			.addPreferredGap(ComponentPlacement.UNRELATED)
		  			.addComponent(addPerson)
		  			.addPreferredGap(ComponentPlacement.UNRELATED)
		  			.addComponent(removePerson)
		  			.addContainerGap(64, Short.MAX_VALUE))
		  		.addGroup(Alignment.TRAILING, gl_pers.createSequentialGroup()
		  			.addContainerGap(347, Short.MAX_VALUE)
		  			.addComponent(viewPersons)
		  			.addGap(328))
		  );
		  gl_pers.setVerticalGroup(
		  	gl_pers.createParallelGroup(Alignment.LEADING)
		  		.addGroup(gl_pers.createSequentialGroup()
		  			.addGroup(gl_pers.createParallelGroup(Alignment.LEADING)
		  				.addGroup(gl_pers.createSequentialGroup()
		  					.addGap(5)
		  					.addComponent(pers1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
		  				.addGroup(gl_pers.createSequentialGroup()
		  					.addGap(11)
		  					.addGroup(gl_pers.createParallelGroup(Alignment.BASELINE)
		  						.addComponent(addPerson)
		  						.addComponent(removePerson))))
		  			.addGap(18)
		  			.addComponent(viewPersons)
		  			.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		  );
		  pers.setLayout(gl_pers);
		  
		  persons = new JScrollPane();
		  pTable = new JTable();
		  pTable.setFillsViewportHeight(true);
		  frame.getContentPane().add(persons);
		  persons.setViewportView(pTable);
		  
		  
		  
		  frame.getContentPane().add(persons);
		  frame.getContentPane().add(acc);
		  aPName = new JTextField("name                          ");
		  aPName.setFont(new Font("Sans Serif", Font.BOLD,20));
		  aPId = new JTextField("person_id");
		  aPId.setFont(new Font("Sans Serif", Font.BOLD,20));
		  aId = new JTextField("account_id");
		  aId.setFont(new Font("Sans Serif", Font.BOLD,20));
		  spending = new JCheckBox();
		  spending.setText("Spending Account");
		  saving = new JCheckBox();
		  saving.setText("Saving Account");
		  viewAccounts = new JButton("View all accounts");
		  
		  viewAccounts.addActionListener(
				  new ActionListener()
				  {
					public void actionPerformed(ActionEvent ae) {
						aTable.setModel(createTableAccounts(bank.getAllAccounts()));
					}
					  
				  }
				  );
		  GroupLayout gl_acc = new GroupLayout(acc);
		  gl_acc.setHorizontalGroup(
		  	gl_acc.createParallelGroup(Alignment.LEADING)
		  		.addGroup(gl_acc.createSequentialGroup()
		  			.addGroup(gl_acc.createParallelGroup(Alignment.LEADING)
		  				.addGroup(gl_acc.createSequentialGroup()
		  					.addGap(242)
		  					.addComponent(addAccount)
		  					.addGap(40)
		  					.addComponent(removeAccount))
		  				.addGroup(gl_acc.createSequentialGroup()
		  					.addContainerGap()
		  					.addGroup(gl_acc.createParallelGroup(Alignment.TRAILING)
		  						.addComponent(viewAccounts)
		  						.addGroup(gl_acc.createSequentialGroup()
		  							.addComponent(aPName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
		  							.addPreferredGap(ComponentPlacement.UNRELATED)
		  							.addComponent(aPId, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
		  							.addPreferredGap(ComponentPlacement.RELATED)
		  							.addComponent(aId, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
		  					.addPreferredGap(ComponentPlacement.UNRELATED)
		  					.addComponent(spending)
		  					.addGap(18)
		  					.addComponent(saving)))
		  			.addContainerGap(85, Short.MAX_VALUE))
		  );
		  gl_acc.setVerticalGroup(
		  	gl_acc.createParallelGroup(Alignment.LEADING)
		  		.addGroup(gl_acc.createSequentialGroup()
		  			.addContainerGap()
		  			.addGroup(gl_acc.createParallelGroup(Alignment.BASELINE)
		  				.addComponent(removeAccount)
		  				.addComponent(addAccount))
		  			.addPreferredGap(ComponentPlacement.RELATED)
		  			.addGroup(gl_acc.createParallelGroup(Alignment.BASELINE)
		  				.addComponent(aPName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
		  				.addComponent(aPId, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
		  				.addComponent(aId, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
		  				.addComponent(spending)
		  				.addComponent(saving))
		  			.addPreferredGap(ComponentPlacement.UNRELATED)
		  			.addComponent(viewAccounts)
		  			.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		  );
		  acc.setLayout(gl_acc);
		  frame.getContentPane().add(accounts);
		  
		  	  aTable = new JTable();
		  	  accounts.setViewportView(aTable);
		  	  aTable.setFillsViewportHeight(true);
		  frame.setVisible(true);
		  
		  
	  }

	private DefaultTableModel createTable(Set<Object> set) {
		
		DefaultTableModel model = new DefaultTableModel();
		int count=0;
		for(Object o: set)
		{
			c = o.getClass();
			break;
		}
        for(Field field : c.getDeclaredFields())
		{
           model.addColumn(field.getName());
           count++;
		}
        String[] row = new String [count];
        for(Object e : set )
        {   
        	count = 0;
        	 for(Field field : e.getClass().getDeclaredFields())
     		{
                try {
                	field.setAccessible(true);
                	System.out.println(field.get(e).toString());
					row[count++]= field.get(e).toString();
				} catch (IllegalArgumentException e1) {
					e1.printStackTrace();
				} catch (IllegalAccessException e1) {
					e1.printStackTrace();
				}
     		}
        	model.addRow(row);
	    }
        model.fireTableDataChanged();
        return model;
	}
private DefaultTableModel createTableAccounts(Set<Account> set) {
		
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("id");
		model.addColumn("balance");
		model.addColumn("interest rate");
         
        String[] row = new String [3];
        for(Account e : set )
        {   
        	row[0] = ((Integer)e.getId()).toString();
        	row[1] = ((Double)e.getBalance()).toString();
        	if(e  instanceof SavingAccount )
        	row[2] =  ((Double)((SavingAccount)e).getInterestRate()).toString();
        	else
        		row[2] = "0.0"
        		;
        	model.addRow(row);
	    }
        model.fireTableDataChanged();
        return model;
	}

}
