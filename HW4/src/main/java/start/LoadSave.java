package start;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import account.Account;
import account.Person;

public class LoadSave {

public static void save(Map<Person, Set<Account>> bank, String fileName )
{
	try {
		FileOutputStream f = new FileOutputStream(fileName);
		ObjectOutputStream o = new ObjectOutputStream(f);
		o.writeObject(bank);
		o.close();
		f.close();
		
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
}

@SuppressWarnings("unchecked")
public static Map<Person, Set<Account>> load(String fileName)
{
	
	try {
		FileInputStream f = new FileInputStream(fileName);
		ObjectInputStream o = new ObjectInputStream(f);
		Map<Person, Set<Account>> bank = (HashMap<Person, Set<Account>>)o.readObject();
		o.close();
		f.close();
		for( Entry<Person, Set<Account>> entry: bank.entrySet())
		{
			for(Account a : entry.getValue())
				a.addObserver(entry.getKey());
		}
		return bank;
	} catch (FileNotFoundException e) {
		System.out.println(e);
		return null;
	} catch (IOException e) {
		System.out.println(e);
		return null;
	} catch (ClassNotFoundException e) {
		e.printStackTrace();
		return null;
	}
}

}
