package bank;


import java.util.Set;

import account.Account;
import account.Person;

public interface BankProc {

	/*
	 * Adds a new person
	 * @param name Name of the Person to be added
	 * @param id Id of the Person to be added
	 * @pre name!=null && bank wellFormed && !bank.contains person
	 * @post init bank size == final bank size - 1 && bank wellFormed
	 */
	public void addPerson(String name, int id);
	
	
	/*
	 * Deletes a person
	 * @param name - name of the person to be added
	 * @param id - id of the person to be added
	 * @pre name!=null && bank wellFormed && bank contains person
	 * @post init bank size == final bank size - 1 && bank wellFormed
	 */
	public void deletePerson(String name, int id);
	
	
	/*
	 * Adds a new account for a person
	 * @param aId Id of the account to be added
	 * @param spending - true if account to be added is a spending one
	 * @param name - name of the person
	 * @param id - id of the person
	 * @pre bank wellFormed && bank.contains person
	 * @post initialSize account ==  finalSize account - 1 && bank wellFormed
	 */
	public void addAccountForPerson(int aId, boolean spending,String name, int id);
	
	
	/*
	 * Deletes an account for a person
	 * @param aId Id of the account to be deleted
	 * @param name - name of the person
	 * @param id - id of the person
	 * @pre bank wellFormed && bank.contains(person) && bank.get(person).contains(account)
	 * @post initialSize account ==  finalSize account + 1 && bank wellFormed
	 */
	public void deleteAccountForPerson(int aId, String name, int id);
	
	
	/*
	 * Returns a set of all the persons in the bank
	 * @pre bank wellFormed 
	 * @post set to be returned != null
	 * @return set of persons
	 */
	public Set<Person> getAllPersons();
	
	
	/*
	 * Returns a set of all the accounts associated to one person
	 * @pre name!=null && bank wellFormed
	 * @post set to be returned != null
	 * @return set of accounts
	 */
	public Set<Account> getAllAccountsForPerson(String name, int id);
	
	
	/*
	 * generates a bank report
	 * @pre bank wellFormed
	 */
	public void generateReport();
}
