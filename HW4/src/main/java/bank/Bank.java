package bank;

import java.io.Serializable;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import account.Account;
import account.Person;
import account.SavingAccount;
import account.SpendingAccount;

public class Bank implements BankProc, Serializable{
	private static final long serialVersionUID = 1L;
	private Map<Person,Set<Account>> bank;
	
	public Bank()
	{
		bank = new HashMap<Person, Set<Account>>();
	}

	public void addPerson(String name, int id) 
	{
		assert isWellFormed(): "Bank is not well formed";
		assert name!=null : "not a valid person name";
		Person p = new Person(name, id);
		assert !bank.containsKey(p) : "Person appears already as an account holder ";
		
		System.out.println(p);
		if (bank.containsKey(p)) return;
		int initialSize = bank.size();
		bank.put(p, new HashSet<Account>());
		int finalSize = bank.size();
		
		assert isWellFormed(): "Bank is not well formed after adding a person";
		assert finalSize == initialSize + 1 : "Failed adding the person";
	}
	
	public void deletePerson(String name, int id)
	{
		assert isWellFormed(): "Bank is not well formed";
		assert name!=null : "not a valid person name";
		Person p = new Person(name, id);
		assert bank.containsKey(p) : "There is no such an account holder";
		
		if(!bank.containsKey(p)) return;
		int initialSize = bank.size();
		bank.remove(p);
		int finalSize = bank.size();
		
		assert isWellFormed(): "Bank is not well formed after deleting a person";
		assert finalSize == initialSize - 1 : "Failed deleting the person";
	}

	public boolean isWellFormed()
	{
		if (bank == null) return false;
		for (Entry<Person, Set<Account>> e : bank.entrySet() )
		{
			if(e.getKey() == null || e.getValue() == null)  return false;
			if(!(e.getKey() instanceof Person)) return false;
			if(e.getKey().getName() == "" || e.getKey().getId()<=0) return false;
			if(!isNameValid(e.getKey().getName())) return false;
			for (Account a : e.getValue())
				if(a.getId()<=0) return false;
		}
		return true;
	}
	
	private boolean isNameValid(String name){
	     StringCharacterIterator iterator = new StringCharacterIterator(name);
	     char c =  iterator.current();
	     while (c != StringCharacterIterator.DONE )
	     {
	      if (!Character.isLetter(c) && !Character.isSpaceChar(c))
	      {
	    	  return false;
	      }
	       c = iterator.next();
	     }
	     return true;
	  }
	
	public void addAccountForPerson(int aId, boolean spending, String name, int id) {
		assert isWellFormed() : "Bank is not well formed";
		assert name != null : "Not a valid person name";
		Person p = new Person(name, id);
		Account a;
	    if(spending)
	      a = new SpendingAccount(aId);
	    else 
	    	a = new SavingAccount(aId);
		assert bank.containsKey(p) : "inexistent account holder"; 
		
		if(!bank.containsKey(p)) return;
		int initialSize = bank.get(p).size();
		if(!bank.containsKey(p)) return;
		a.addObserver(p); 
	    bank.get(p).add(a);
	    int finalSize = bank.get(p).size();
	    
		assert initialSize == finalSize - 1 : "Adding the account was unsuccesful"; 
		assert isWellFormed() : "Bank is not well formed after adding an account";
	}

	public void deleteAccountForPerson(int aId, String name, int id) {
		assert isWellFormed() : "Bank is not well formed";
	    assert name != null : "Not a valid person name"; 
	    Person p = new Person(name, id);
	    Account a = new Account(aId);
	    assert bank.containsKey(p): "inexistent account holder";
	    assert bank.get(p).contains(a) : "No such account";

	      
	    if(!bank.containsKey(p)) return;
	    int initialSize = bank.get(p).size(); //number of accounts for a before deleting
	     bank.get(p).remove(a);
	    int finalSize = bank.get(p).size();;
	    
	    assert initialSize == finalSize + 1 : "Deleting the account was unsuccesful"; 
		assert isWellFormed() : "Bank is not well formed after deleting an account";	
	}

	public Set<Person> getAllPersons() 
	{
		assert isWellFormed() : "Bank is not well formed";
		
		Set<Person> persons = bank.keySet();
		
		assert persons != null : "couldn't find any person";
		return persons;
	}
	
	public Set<Account> getAllAccounts() 
	{
		assert isWellFormed() : "Bank is not well formed";
		
		Set<Account> accounts = new HashSet<Account>();
		for (Entry<Person, Set<Account>> entry : bank.entrySet())
			accounts.addAll(entry.getValue());
		
		assert accounts != null : "couldn't find any person";
		return accounts;
	}

	public Set<Account> getAllAccountsForPerson(String name, int id) {
		
		assert isWellFormed() : "Bank is not well formed";
		assert name != null : "not a valid person name";
		Person p = new Person(name, id);
		assert bank.containsKey(p) : "Person doesn't appear as an account holder";
		
		Set<Account> accounts = null;
		accounts = bank.get(p);
		
		assert accounts != null : "couldn't find any account associated to this person";
		return accounts;
	}

	public void generateReport() {
		System.out.println("-------------------------------------------");
		 for (Entry<Person, Set<Account>> entry : sort())
		 {
			 System.out.println("");
			 System.out.print(entry.getKey());
			 for (Account a :  entry.getValue())
			 {
				 if (a instanceof SavingAccount)
					 System.out.println((SavingAccount)a);
				 else
					 System.out.println((SpendingAccount)a);
			 }
		 }	
	}

	public Map<Person,Set<Account>> getBank() {
		return bank;
	}

	public void setBank(Map<Person,Set<Account>> bank) {
		this.bank = bank;
	}
   
	public ArrayList<Entry< Person,Set<Account>>> sort(){
		 List < Entry<Person, Set<Account>>> sorted = new ArrayList<Entry< Person,Set<Account>>>();
		 for (Entry<Person, Set<Account>> entry : bank.entrySet())
			   sorted.add(entry);
		 Collections.sort(sorted, new Comparator< Map.Entry<Person,Set<Account>>>()
		    {
		        public int compare(Entry<Person,Set<Account>> e1, Entry<Person,Set<Account>> e2)
		        {
		        	Integer i1 = (Integer)e1.getKey().getId();
		        	Integer i2 = (Integer)e2.getKey().getId();
		            return i1.compareTo(i2);
		        } 
		   });
		 return (ArrayList<Entry<Person, Set<Account>>>) sorted;
	}
}
